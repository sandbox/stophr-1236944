<?php
/**
 * @file
 */

/**
  * Implementation of views_plugin_style().
  */
class activity_charts_style_plugin extends views_plugin_style {

  private $supportedCharts = array(ACTIVITY_CHARTS_LINE => 'Line Chart', ACTIVITY_CHARTS_PIE_USER => 'Pie Chart (user)', ACTIVITY_CHARTS_PIE_NODE => 'Pie Chart (node)');
  
  function init(&$view, &$display) {
    parent::init($view, $display);
    
    $display->handler->definition['use pager'] = FALSE;
  }	

  function option_definition() {
    $options = parent::option_definition();
    
    $options['activity'] = array('default' => 0);
    $options['activity_age'] = array('default' => 30);
    $options['chart']['type'] = array('default' => 'line');
    $options['chart']['size']['width'] = array('default' => 600);
    $options['chart']['size']['height'] = array('default' => 200);
    $options['chart']['background']['color'] = array('default' => 'FFFFFF');
    $options['chart']['background']['transparency'] = array('default' => 100);
    $options['chart']['chart']['color'] = array('default' => 'FFFFFF');
    $options['chart']['chart']['transparency'] = array('default' => 0);
    
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['activity'] = array(
      '#type' => 'select',
      '#title' => t('Activity publisher template(s)'),
      '#description' => t('The activity publisher template(s) you want to create analytics for'),
      '#required' => TRUE,
      '#options' => array(),
      '#multiple' => FALSE,
      '#default_value' => $this->options['activity'],
    );
    
    $sql = 'SELECT ta.hook, ta.aid, a.description FROM {trigger_assignments} ta 
            INNER JOIN {actions} a ON a.aid = ta.aid WHERE a.type = "activity"';
            
    $query = db_query($sql);
    while ($result = db_fetch_array($query)) {
      $form['activity']['#options'][$result['aid']] = $result['description'];
    }

    $form['activity_age'] = array(
      '#default_value'  => $this->options['activity_age'],
      '#description'    => t('How recent the activity should be, in days.'),
      '#required'       => TRUE,
      '#size'           => 8,
      '#type'           => 'textfield',
      '#title'          => t('Maximum age of activity'),
    );
    $form['chart'] = array(
      '#type' => 'fieldset',
      '#title' => t('Chart settings'),
      '#tree' => TRUE,
    );
    $form['chart']['type'] = array(
      '#type' => 'select',
      '#title' => t('Chart Type'),
      '#description' => t('The type of the chart you want displayed'),
      '#options' => $this->supportedCharts,
      '#default_value' => $this->options['chart']['type'],
    );
    $form['chart']['size'] = array(
      '#type' => 'fieldset',
      '#title' => t('Size settings'),
      '#tree' => TRUE,
    );
    $form['chart']['size']['width'] = array(
      '#default_value'  => $this->options['chart']['size']['width'],
      '#description'    => t('The chart width, in pixels'),
      '#required'       => TRUE,
      '#size'           => 8,
      '#type'           => 'textfield',
      '#title'          => t('Width'),
    );
    $form['chart']['size']['height'] = array(
      '#default_value'  => $this->options['chart']['size']['height'],
      '#description'    => t('The chart height, in pixels'),
      '#required'       => TRUE,
      '#size'           => 8,
      '#type'           => 'textfield',
      '#title'          => t('Height'),
    );
    $form['chart']['background'] = array(
      '#type' => 'fieldset',
      '#title' => t('Background color'),
      '#tree' => TRUE,
    );
    $form['chart']['background']['color'] = array(
      '#default_value'  => $this->options['chart']['background']['color'],
      '#description'    => t('The hexdecimal value of the background color.'),
      '#required'       => TRUE,
      '#size'           => 6,
      '#maxlength'      => 6,
      '#type'           => 'textfield',
      '#title'          => t('Background color'),
      '#field_prefix'   => '#',
    );
    $form['chart']['background']['transparency'] = array(
      '#default_value'  => $this->options['chart']['background']['transparency'],
      '#description'    => t('The percentage of transparency applied to the background color.'),
      '#required'       => TRUE,
      '#size'           => 3,
      '#maxlength'      => 3,
      '#type'           => 'textfield',
      '#title'          => t('Background transparency'),
      '#field_suffix'   => '%',
    );
    $form['chart']['chart'] = array(
      '#type' => 'fieldset',
      '#title' => t('Chart area color'),
      '#tree' => TRUE,
    );
    $form['chart']['chart']['color'] = array(
      '#default_value'  => $this->options['chart']['chart']['color'],
      '#description'    => t('The hexdecimal value of the color of the chart area.'),
      '#required'       => TRUE,
      '#size'           => 6,
      '#maxlength'      => 6,
      '#type'           => 'textfield',
      '#title'          => t('Chart area color'),
      '#field_prefix'   => '#',
    );
    $form['chart']['chart']['transparency'] = array(
      '#default_value'  => $this->options['chart']['chart']['transparency'],
      '#description'    => t('The percentage of transparency applied to the color of the chart area.'),
      '#required'       => TRUE,
      '#size'           => 3,
      '#maxlength'      => 3,
      '#type'           => 'textfield',
      '#title'          => t('Chart area transparency'),
      '#field_suffix'   => '%',
    );
  }
  
  /**
   * Make sure the display and all associated handlers are valid.
   *
   * @return
   *   Empty array if the display is valid; an array of error strings if it is not.
   */
  function validate() {
    $errors = parent::validate();
    
    if ((!is_array($this->options['activity']) && (intval($this->options['activity']) == 0)) || (count($this->options['activity']) == 0)) {
      $errors[] = t('You have to select at least one activity publisher template to track.');
    }
    
    if (!in_array($this->options['chart']['type'], array_keys($this->supportedCharts))) {
      $errors[] = t('You selected an invalid chart type.');
    }
    
    if ($this->options['activity_age'] <= 0) { 
      $errors[] = t('You entered an invalid activity age.');
    }
    
    if (!activity_charts_color_validate($this->options['chart']['background']['color'])) {
      $errors[] = t('You entered an invalid background color.');
    }
    
    if (!activity_charts_color_transparency_validate($this->options['chart']['background']['transparency'])) {
      $errors[] = t('You entered an invalid background transparency value.');
    }
    
    if (!activity_charts_color_validate($this->options['chart']['chart']['color'])) {
      $errors[] = t('You entered an invalid chart area color.');
    }
    
    if (!activity_charts_color_transparency_validate($this->options['chart']['chart']['transparency'])) {
      $errors[] = t('You entered an invalid chart area transparency value.');
    }
    
    return $errors;
  }
  
  function uses_row_plugin() {
    return FALSE;
  }
  
  function uses_fields() {
    return FALSE;
  }
  
  function query() {
    $this->view->query->clear_fields();
    $this->view->query->orderby = array();
    
    $this->view->query->add_field('activity', 'aid', 'activity_aid', array('count' => TRUE));
    $this->view->query->add_field('activity', 'created', 'activity_created', array('aggregate' => TRUE));
    
    $this->view->query->add_having(0, '((UNIX_TIMESTAMP(now()) - activity_created) / 86400) <= '. $this->options['activity_age']);
    
    $this->view->query->set_where_group('OR', 1);
    
    if (is_array($this->options['activity'])) {
      foreach($this->options['activity'] as $aid) {
        $this->view->query->add_where(1, 'activity.actions_id = '.intval($aid));
      }
    }
    else {
      $aid = intval($this->options['activity']);
      $this->view->query->add_where(1, 'activity.actions_id = '. $aid);
    }
    
    switch ($this->options['chart']['type']) {
      case ACTIVITY_CHARTS_PIE_USER:
        $this->view->query->add_field('activity', 'uid', 'activity_id');
        $this->view->query->add_field('users_activity', 'name', 'activity_id_name');
        
        $this->view->query->add_groupby('activity_id');
        
        break;
      case ACTIVITY_CHARTS_PIE_NODE:
        $query = db_query("SELECT hook, op FROM {trigger_assignments} WHERE aid = '%s'", $aid);
        $info = db_fetch_object($query);
        
        if (!empty($info) && ($module = activity_module_name($info->hook)) && module_invoke($module, 'activity_charts_query_alter', $this->view->query)) {
          // Activity implementation adds activity_id and activity_id_name field
        }
        else {
          $this->view->query->add_field('activity', 'nid', 'activity_id');
          $this->view->query->add_field('node_activity', 'title', 'activity_id_name');
        }
        
        $this->view->query->add_groupby('activity_id');
        
        break;
      case ACTIVITY_CHARTS_LINE:
        $this->view->query->add_orderby('activity', 'created', 'ASC');
        
        $this->view->query->add_groupby('YEAR(FROM_UNIXTIME(activity_created))');
        $this->view->query->add_groupby('MONTH(FROM_UNIXTIME(activity_created))');
        $this->view->query->add_groupby('DAY(FROM_UNIXTIME(activity_created))');
        
        break;
    }
  }
  
  function render() {
    if ($this->options['chart']['type'] == ACTIVITY_CHARTS_LINE) {
      $display->handler->options['defaults']['items_per_page'] = FALSE;
      $display->handler->options['items_per_page'] = 0;
    }
    
    return parent::render();
  }
  
}
