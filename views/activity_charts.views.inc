<?php

/**
 * @file: provides views data integration for activity module as the base table
 */

/**
 * Implementation of hook_views_plugins().
 */
function activity_charts_views_plugins() {
  return array(
    'module' => 'activity_charts',
    'style' => array(
      'activity_chart' => array(
        'title' => t('Activity Chart'),
        'theme' => 'activity_charts_view',
        'help' => t('Display activity items as a chart'),
        'handler' => 'activity_charts_style_plugin',
        'path' => drupal_get_path('module', 'activity_charts') .'/views',
        'base' => array('activity'),
        'uses options' => TRUE,
        'uses row plugin' => FALSE,
        'type' => 'normal',
      ),
    ),
  );
}